Exercises (a.k.a. Tutorials)
============================

.. toctree::
   :hidden:

   03_queries
   4-triggers/index

This year's exercises will be picked from old exams.

2015-01-27 Exercise 1: Design.
  :download:`Questions <Ex1-questions.pdf>`,
  :download:`Solutions <Ex1-solutions.pdf>`
2015-02-10 Exercise 2: Design.
  :download:`Questions <Ex2-questions.pdf>`,
  :download:`Solutions <Ex2-solutions.pdf>`
2015-02-17 Exercise 3:
  :doc:`03_queries`
2015-02-24 Exercise 4:
  :doc:`4-triggers/index`
2015-03-03 Exercise 5: Questions 6 - Exam  on 13 January 2015  & Question 6 - Exam on 9 March 2010
  :download:`Slides <Transactionslides.pdf>`
