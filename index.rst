Databases (|version|)
=====================

.. sidebar:: Staff VT2015

    - `Aarne Ranta <http://www.cse.chalmers.se/~aarne/>`_ (Examiner)
    - `Evgenii Kotelnikov <http://www.chalmers.se/en/staff/Pages/evgenyk.aspx>`_
    - `Grégoire Détrez <http://www.gu.se/english/about_the_university/staff/?languageId=100001&userId=xdetgr>`_
    - `Hamid Ebadi Tavallaei <http://www.chalmers.se/en/staff/Pages/hamide.aspx>`_
    - `Herbert Lange <http://www.gu.se/english/about_the_university/staff/?languageId=100001&userId=xlangh&departmentId=107824>`_
    - `Per Hallgren <http://www.chalmers.se/en/staff/Pages/hallgrep.aspx>`_

Chalmers University of Technology course code:
`TDA357 <https://student.portal.chalmers.se/en/chalmersstudies/courseinformation/Pages/SearchCourse.aspx?course_id=21851&parsergrp=3>`__

University of Gothenburg course code:
`DIT620 <http://gul.gu.se/public/courseId/71533/lang-en/publicPage.do>`__


News
~~~~
2016-02-24
  Solutions for the fourth tutorial are now available
  :ref:`here <solution-tutorial4>`.
2016-02-22
  Solutions for tutorials 1 and 2 are now available.
2016-02-20
  The solution to this week's exercises is now available
  :ref:`here <solution-tutorial3>`.
2016-02-16
  The :doc:`exercises for this week <tutorials/03_queries>` have been changed.
2016-02-15
  :doc:`Exercises for this week <tutorials/03_queries>` are now available.
2016-02-03
  Exercises for the second tutorial are now available, see
  :doc:`tutorials/index`.
2016-01-20
  The web page should now be up to date for this year's course.
  If you find any obsolete information please let us know!
  Some information of course remains to be completed. 
  In particular, the lecture notes and the weekly exercises.

2016-01-04
  Contrary to what was previously announced, the first lecture is on
  **Wednesday 20 January** at 13:15 in HB2.

2015-12-16
  First version of Spring Term 2016 web page. **Based on copying last
  year's course page, therefore not yet reliable.**

Information
~~~~~~~~~~~

.. toctree::
   :maxdepth: 1

   slides/index
   tutorials/index
   lab/index
   course_book
   pgtips/index
   xml/index
   exam/index
   Schedule <https://se.timeedit.net/web/chalmers/db1/public/ri157XQQ709Z50Qv17043gZ6y6Y7002Q5Y61Y5.html>
   Course Google Group <https://groups.google.com/forum/#!forum/tda357-vt2016>
   Student Portal Entry <https://student.portal.chalmers.se/en/chalmersstudies/courseinformation/Pages/SearchCourse.aspx?course_id=21851&parsergrp=3>
   Fire submission system <https://databases-vt16.fire.cse.chalmers.se/>
